from sklearn.datasets import fetch_olivetti_faces


oli = fetch_olivetti_faces()
h=64
w=64
X = oli.data

X_t=X[20]
X=X[:-1]

X_m=np.mean(X,axis=0)
X=X-X_m # center them
X_t=X_t-X_m

def plot_unexplained_var():
    for k in range(200):
    model = PCA(n_components = k+1)
    model.fit(X)

    unexplained_vars = 1 - np.cumsum(model.explained_variance_ratio_)

    pylab.plot(unexplained_vars)
    pylab.xlim(0,200)
    pylab.xlabel("Principle Components")
    pylab.ylabel("Unexplained Variance")

def eigenfaces():
    component_num = [1,2,3,4,5,30,50,100]
    for i in range(8):
        p_component = model.components_[component_num[i]-1]
        image = np.reshape(p_component, (h,w))
        figure()
        plt.title("Principle Component %d" %component_num[i])
        imshow(image, cmap=cm.Greys_r)

def PCA_transform():
    num_comps = [1,10,20,30,40,50,60,70,80,90,100,120,140,160,180,200]
    for i in num_comps:
    model = PCA(n_components = i)
    model.fit(X)
    restored = model.inverse_transform(model.transform(X_t.reshape(1,-1)))
    image = np.reshape(restored, (h,w))
    figure()
    plt.title("%d PCA features" %i)
    imshow(image, cmap=cm.Greys_r)
