import numpy as np
from sklearn import neighbors
from mpl_toolkits.mplot3d import Axes3D
import random
from sklearn.decomposition import PCA
from PIL import Image
from sklearn.cluster import KMeans
import scipy.spatial.distance as dist
from matplotlib.colors import ListedColormap

enable_interactive=False
if enable_interactive:
    from IPython.display import display
    from IPython.html.widgets import interact

numpy.random.seed(seed=2232017)
true_cov = np.array([[1,.5,.2],[.5,1,.3],[.2,.3,1]])
data=(np.random.randn(1000,3)).dot(np.linalg.cholesky(true_cov).T)


def plot_orig():
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.scatter(data[:,0],data[:,1],data[:,2])
    if enable_interactive:
        @interact(elev=(-90, 90), azim=(0, 360))
        def view(elev, azim):
            ax.view_init(elev, azim)
            display(ax.figure)

def pcaeig(data):
    cov = np.dot(np.transpose(data), data) * (1/data.shape[0])
    eigen_vals, eigen_vects = np.linalg.eigh(cov)
    eigen_vals = eigen_vals[::-1]
    eigen_vects = np.fliplr(eigen_vects)
    return eigen_vects, eigen_vals

def plot_principal_comp()
    W, s = pcaeig(data
    figb = plt.figure()
    axb = Axes3D(figb)
    axb.scatter(data[:,0],data[:,1],data[:,2],alpha=0.1)
    c=['r-','g-','y-']
    for var, pc,color in zip(s, W,c):
        axb.plot([0, 2*var*pc[0]], [0, 2*var*pc[1]], [0, 2*var*pc[2]], color, lw=2)
    if enable_interactive:
        @interact(elev=(-90, 90), azim=(0, 360))
        def view(elev, azim):
            axb.view_init(elev, azim)
            display(axb.figure)

def pcaimreduce(data, W, k):
    Wk = W[0:k]
    features = np.zeros((data.shape[0], k))
    for i in range(data.shape[0]):
        features[i] = np.dot(Wk, data[i])
    return features

def pcareconstruct(pcadata,W,k):
    Wk = np.transpose(W[0:k])
    features = np.zeros((pcadata.shape[0], W.shape[0]))
    for i in range(pcadata.shape[0]):
        features[i] = np.dot(Wk, pcadata[i])
    return features

def plot2D_reduction(data, W, 2):
    reduced = pcadimreduce(data, W, 2)
    reconstructure_data = pcareconstruct(reduced, W, 2)
    scatter(reduced[:,0], reduced[:,1])

def visualize_two_PC(reconstructed_data):
    figc = plt.figure()
    axc = Axes3D(figc)
    axc.scatter(reconstructed_data[:,0],reconstructed_data[:,1],reconstructed_data[:,2],alpha=0.1)
    c=['r-','g-','y-']
    for var, pc,color in zip(s, W,c):
        axc.plot([0, 2*var*pc[0]], [0, 2*var*pc[1]], [0, 2*var*pc[2]], color, lw=2)

    if enable_interactive:
        @interact(elev=(-90, 90), azim=(0, 360))
        def view(elev, azim):
            axc.view_init(elev, azim)
            display(axc.figure)

def visualize_orig_feature_space(reconstructed_data):
    figd = plt.figure()
    axd = Axes3D(figd)
    axd.scatter(reconstructed_data_1[:,0],reconstructed_data_1[:,1],reconstructed_data_1[:,2],alpha=0.1)
    c=['r-','g-','y-']
    for var, pc,color in zip(s, W,c):
        axd.plot([0, 2*var*pc[0]], [0, 2*var*pc[1]], [0, 2*var*pc[2]], color, lw=2)

    if enable_interactive:
        @interact(elev=(-90, 90), azim=(0, 360))
        def view(elev, azim):
            axd.view_init(elev, azim)
            display(axd.figure)
