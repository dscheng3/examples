import numpy as np
import utils
import random


class Agent:

    def __init__(self, actions, Ne, C, gamma):
        self.actions = actions
        self.Ne = Ne # used in exploration function
        self.C = C
        self.gamma = gamma
        # Create the Q and N Table to work with
        self.Q = utils.create_q_table()
        self.N = utils.create_q_table()
        self.s = None
        self.a = None
        self.points = 0

    def train(self):
        self._train = True

    def eval(self):
        self._train = False

    # At the end of training save the trained model
    def save_model(self,model_path):
        utils.save(model_path, self.Q)

    # Load the trained model for evaluation
    def load_model(self,model_path):
        self.Q = utils.load(model_path)

    def reset(self):
        self.points = 0
        self.s = None
        self.a = None

    def act(self, state, points, dead):
        '''
        :param state: a list of [snake_head_x, snake_head_y, snake_body, food_x, food_y] from environment.
        :param points: float, the current points from environment
        :param dead: boolean, if the snake is dead
        :return: the index of action. 0,1,2,3 indicates up,down,left,right separately
        '''
        def f(u, v):
            if v < self.Ne:
                return 1
            return u
        # discretize the state space
        [snake_head_x, snake_head_y, snake_body, food_x, food_y] = state
        [adjoining_wall_x, adjoining_wall_y]  = [0, 0]
        if snake_head_x == 40:
            adjoining_wall_x = 1
        elif snake_head_x == 480:
            adjoining_wall_x = 2
        if snake_head_y == 40:
            adjoining_wall_y = 1
        elif snake_head_y == 480:
            adjoining_wall_y = 2
        wall = [adjoining_wall_x, adjoining_wall_y]
        [food_dir_x, food_dir_y] = [0, 0]
        if food_x < snake_head_x:
            food_dir_x = 1
        elif food_x > snake_head_x:
            food_dir_x = 2
        if food_y < snake_head_y:
            food_dir_y = 1
        elif food_y > snake_head_y:
            food_dir_y = 2
        food = [food_dir_x, food_dir_y]
        [adjoining_body_top, adjoining_body_bottom, adjoining_body_left, adjoining_body_right] = [0, 0, 0, 0]
        [top, bottom, left, right] = [(snake_head_x, snake_head_y-40), (snake_head_x, snake_head_y+40), (snake_head_x-40, snake_head_y), (snake_head_x+40, snake_head_y)]
        # Go through the parts of the snake body
        for x,y in snake_body:
            if (x, y) == left:
                adjoining_body_left = 1
            if (x, y) == right:
                adjoining_body_right = 1
            if (x, y) == top:
                adjoining_body_top = 1
            if (x, y) == bottom:
                adjoining_body_bottom = 1
        body = [adjoining_body_top, adjoining_body_bottom, adjoining_body_left, adjoining_body_right]
        snake = [snake_head_x, snake_head_y]

        """
        def create_q_table():
        	return np.zeros((NUM_ADJOINING_WALL_X_STATES, NUM_ADJOINING_WALL_Y_STATES, NUM_FOOD_DIR_X, NUM_FOOD_DIR_Y,
        					 NUM_ADJOINING_BODY_TOP_STATES, NUM_ADJOINING_BODY_BOTTOM_STATES, NUM_ADJOINING_BODY_LEFT_STATES,
        					 NUM_ADJOINING_BODY_RIGHT_STATES, NUM_ACTIONS))
        """
        def argmax(actions):
            idxs = [3,2,1,0]
            best = max(actions)
            for idx in idxs:
                if actions[idx] == best:
                    return idx
            return 0
        g = self.gamma
        if self._train:
            if dead:
                R = -1
                delta = []
                alpha = self.C/(self.C+self.N[self.s])
                for i in range(4):
                    current = (adjoining_wall_x, adjoining_wall_y, food_dir_x, food_dir_y, adjoining_body_top, adjoining_body_bottom, adjoining_body_left, adjoining_body_right, i)
                    delta.append(self.Q[current])
                self.Q[self.s] = self.Q[self.s] + alpha * (R + g * max(delta) - self.Q[self.s])
                self.reset()
                return 0
            if not self.s:
                # Do not update Q_table
                a = 3
            else:
                alpha = self.C/(self.C+self.N[self.s])
                R = 0
                if points > self.points:
                    R = 1
                else:
                    R = -0.1
                delta = []
                for i in range(4):
                    current = (adjoining_wall_x, adjoining_wall_y, food_dir_x, food_dir_y, adjoining_body_top, adjoining_body_bottom, adjoining_body_left, adjoining_body_right, i)
                    delta.append(self.Q[current])
                self.Q[self.s] = self.Q[self.s] + alpha * (R + g * max(delta) - self.Q[self.s])
        actions = []
        for a in range(4):
            config = (adjoining_wall_x, adjoining_wall_y, food_dir_x, food_dir_y, adjoining_body_top, adjoining_body_bottom, adjoining_body_left, adjoining_body_right, a)
            if self._train:
                actions.append(f(self.Q[config], self.N[config]))
            else:
                actions.append(self.Q[config])
        a = argmax(actions)
        configuration = (adjoining_wall_x, adjoining_wall_y, food_dir_x, food_dir_y, adjoining_body_top, adjoining_body_bottom, adjoining_body_left, adjoining_body_right, a)
        if self._train:
            self.s = configuration
            self.a = a
            self.points = points
            self.N[configuration] += 1
        return a
