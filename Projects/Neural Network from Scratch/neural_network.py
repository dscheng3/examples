import numpy as np

"""
    Minigratch Gradient Descent Function to train model
    Args:
        epoch (int) - number of iterations to run through neural net
        w1, w2, w3, w4, b1, b2, b3, b4 (numpy arrays) - starting weights
        x_train (np array) - (n,d) numpy array where d=number of features
        y_train (np array) - (n,) all the labels corresponding to x_train
        num_classes (int) - number of classes (range of y_train)
        shuffle (bool) - shuffle data at each epoch if True. Turn this off for testing.
    Returns:
        w1, w2, w3, w4, b1, b2, b3, b4 (numpy arrays) - resulting weights
        losses (list of ints) - each index should correspond to epoch number
            Note that len(losses) == epoch
"""
def minibatch_gd(epoch, w1, w2, w3, w4, b1, b2, b3, b4, x_train, y_train, num_classes, shuffle=False):
    all_accs = []
    idx = list(range(len(y_train)))
    losses = []
    for i in range(epoch):
        loss = 0
        if shuffle:
            np.random.shuffle(idx)
            x_train = x_train[idx]
            y_train = y_train[idx]
        num_batches = int(len( x_train )/200)
        for b_idx in range(1,num_batches + 1):
            start, end = (b_idx-1) * 200, b_idx * 200
            X = x_train[start:end]
            Y = y_train[start:end]
            w1, w2, w3, w4, l = four_nn( X, w1, w2, w3, w4, b1, b2, b3, b4, Y, False)
            loss += l
        losses.append(loss)
        print("current epoch:", i)
    return w1, w2, w3, w4, b1, b2, b3, b4, losses

"""
    Use the trained weights & biases to see how well the nn performs
        on the test data
    Returns:
        avg_class_rate (float) - average classification rate
        class_rate_per_class (list of floats) - Classification Rate per class
            (index corresponding to class number)
"""
def test_nn(w1, w2, w3, w4, b1, b2, b3, b4, x_test, y_test, num_classes):
    avg_class_rate = 0.0
    class_rate_per_class = [0.0] * num_classes
    dp = {}
    for i in range(num_classes):
        dp[i] = 0
    results = four_nn(x_test, w1, w2, w3, w4, b1, b2, b3, b4, y_test, True)
    for i in range(len(y_test)):
        dp[y_test[i]] += 1
        if results[i] == y_test[i]:
            class_rate_per_class[results[i]] += 1
    for i in range(num_classes):
        class_rate_per_class[i]/=dp[i]
    avg_class_rate = sum(class_rate_per_class)/len(class_rate_per_class)
    return avg_class_rate, class_rate_per_class

"""
    4 Layer Neural Network
"""
def four_nn( X, w1, w2, w3, w4, b1, b2, b3, b4, Y, test ):

    eta = 0.1

    Z1, Acache1 = affine_forward( X, w1, b1 )
    A1, Rcache1 = relu_forward( Z1 )

    Z2, Acache2 = affine_forward( A1, w2, b2 )
    A2, Rcache2 = relu_forward( Z2 )

    Z3, Acache3 = affine_forward( A2, w3, b3 )
    A3, Rcache3 = relu_forward( Z3 )

    F, Acache4 = affine_forward( A3, w4, b4 )

    if test:
        classes = []
        for c in F:
            classes.append(np.argmax(c))
        return classes

    loss, dF = cross_entropy( F, Y )

    dA3, dW4, dB4 = affine_backward( dF, Acache4 )
    dZ3 = relu_backward( dA3, Rcache3 )

    dA2, dW3, dB3 = affine_backward( dZ3, Acache3 )
    dZ2 = relu_backward( dA2, Rcache2 )

    dA1, dW2, dB2 = affine_backward( dZ2, Acache2 )
    dZ1 = relu_backward( dA1, Rcache1 )

    dX, dW1, dB1 = affine_backward( dZ1, Acache1 )


    w1 -= eta * dW1
    w2 -= eta * dW2
    w3 -= eta * dW3
    w4 -= eta * dW4

    return w1, w2, w3, w4, loss

"""
    Helper functions
"""
def affine_forward(A, W, b):
    Z = np.add(np.matmul(A, W), b)
    cache = (A, W, b)
    return Z, cache

def affine_backward(dZ, cache):
    dA = np.einsum('ij,kj', dZ, cache[1])
    dW = np.einsum('ij,ik', cache[0], dZ)
    dB = np.sum(dZ, axis=0)
    return dA, dW, dB

def relu_forward(Z):
    A = np.copy(Z)
    A[A<0] = 0
    return A, Z

def relu_backward(dA, cache):
    dA[cache<0] = 0
    return dA

def cross_entropy(F, y):
    dF = np.empty_like(F)
    w = F.shape[1]
    n = y.shape[0]
    loss = 0
    exps = np.exp(F)
    sum_F_ik= np.sum(exps, axis=1)
    for i, y_val in enumerate(y):
        loss += (F[i][int(y_val)] - np.log(sum_F_ik[i]))
    loss = -loss/n

    for i in range(0, n):
        for j in range(0, w):
            grad = 0
            if j == y[i]:
                grad = 1
            dF[i,j] = (grad - (exps[i,j]/sum_F_ik[i]))
    dF = (-1/n) * dF
    #import pdb; pdb.set_trace()

    return loss, dF
